import os
from datetime import datetime

def create_directory(dir_path):
    if dir_path is None:
        return

    new_dir = dir_path + "_" + datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    if os.path.isdir(new_dir) is False:
        os.mkdir(new_dir)
    os.chdir(new_dir)
