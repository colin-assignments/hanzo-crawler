#!/bin/bash

set -e
export PORT=5000

pipenv install --dev
source $(pipenv --venv)/bin/activate

python main.py --seed https://www.independent.ie