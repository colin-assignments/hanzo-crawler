# Hanzo Web Crawler

Written in Python 3

## Tasks

- Write a simple single threaded crawler capable of taking a single seed and collecting all content on a domain into files on disk.
- Collect a small simple site is all that is required but I should be able to run the code.

## Instructions

This can be run be executing `./run-dev`
All dependencies should be loaded when the script is ran.

## #TODO

Would of been nice to add tests, but I was caught for time.