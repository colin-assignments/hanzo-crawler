# -*- coding: utf-8 -*-
import os
import re
import argparse
from urllib import request, parse

from bs4 import BeautifulSoup
from utils import create_directory

class FileDownloader(object):
    """File Downloader class.

    File downloader is responsible for crawling the
    webpage and writing content to disk.
    """

    params = None
    pattern = ""

    def __init__(self, seed_url, params):
        """Create a FileDownloader object with the given options."""
        self.seed_url = seed_url
        self.domain = parse.urlparse(seed_url)[1]

        self._progress = []
        self.params = params

    def extract_info(self):
        """Extract the URLs from the seed page using BeautifulSoup.
        Returns the list of URLs.
        """
        urls = []

        response = request.urlopen(self.seed_url)
        soup = BeautifulSoup(response, 'html.parser')

        for link in soup.findAll('a', attrs={'href': re.compile("^https://" + self.domain)}):
            web_link = link.get('href')
            if web_link.endswith('.html'):
                urls.append(link.get('href'))

        return urls

    def download(self, url_list):
        """Downloads the web page content from each URL and saving locally."""
        dir_path = os.path.join('archives', self.domain)
        create_directory(dir_path)

        for url in url_list:
            filename = str(os.path.basename(url))

            attempts = 0
            while attempts < 3:
                try:
                    response = request.urlopen(url)
                    web_content = response.read().decode(
                        encoding='UTF-8', errors='ignore') # Ignore any errors here

                    f = open(filename, 'w')
                    f.write(web_content)
                    f.close()

                    self._add_progress_list(filename)
                    break
                except request.URLError:
                    attempts += 1

    def _add_progress_list(self, filename):
        self._progress.append(filename)

    def report_progress(self):
        """Reports the number of pages downloaded."""
        pages_downloaded = len(self._progress)
        return '[Progress Report] The number of web pages downloaded is {0}'.format(
            pages_downloaded)


def main(seed_url):
    """Main Method"""
    download_file = FileDownloader(seed_url, {})
    url_list = download_file.extract_info()

    download_file.download(url_list)
    print(download_file.report_progress())


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--seed', default='https://www.example.com',
                        help='seed URL to [%(default)s]')

    args = parser.parse_args()
    main(seed_url=args.seed)
